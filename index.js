class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		
		return this;
	}
	willPass() {
		
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {

		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}

// Class to represent a section of students
class Section{
	// every Section object will be instatiated with an empty array for its students
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}	
	// method for adding a student to this section
	// this will take in the same arguments needed to instantiate a Student object
	addStudent(name, email, grades){
		// a Student object will be instantiated before being pushed to the students property
		this.students.push(new Student(name, email, grades));
		// return the Section object afterwards, allowing us to chain this method
		return this;
	}
	// method for computing how many students in the section are honor students
	countHonorStudents(){
		let count = 0;

		this.students.forEach((student) => {
			// 
			if (student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;
			}
		})
		this.honorStudents = count;
		return this;
	}
	computeHonorsPercentage(){
		
		this.honorsPercentage = (this.honorStudents / this.students.length)*100;
		
		return this;
	}
}

class Grade{
	constructor(number){
		this.level = 1;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(name){
		let sections = [];

		this.sections.find((name) => {
			if (typeof name === "string"){
			this.name.push(new Section(name))
			}
		});
		return this;
	}

	// countStudents(){
	// 	let count = 0;

	// 	this.sections.forEach((section) => {
	// 		// 
	// 		if (student.computeAve().willPass().willPassWithHonors().passedWithHonors){
	// 			count++;
	// 		}
	// 	})
	// 	this.honorStudents = count;
	// 	return this;
	// }
}


const grade1 = new Grade('grade1');
console.log(grade1);
const section1A = new Section('section1A').addStudent('John','john@mail.com',[89, 84, 78, 88]);

const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");


console.log(section1A);
console.log(section1B);
console.log(section1C);
console.log(section1D);